(defvar *paja-db* (merge-pathnames ".config/paja-db.txt" (user-homedir-pathname)))

(defparameter *color* t)

(defparameter *colors* '(("red" (format nil "~c~a" #\esc "[31m"))
                         ("green" (format nil "~c~a" #\esc "[32m"))
                         ("yellow" (format nil "~c~a" #\esc "[33m"))
                         ("cyan" (format nil "~c~a" #\esc "[36m"))
                         ("blue" (format nil "~c~a" #\esc "[34m"))
                         ("white" (format nil "~c~a" #\esc "[0m"))))


 (defun main ()
   (arguments *paja-db*
             #+clozure *command-line-argument-list*
             #+clisp ext:*args*
             #+ecl (ext:command-args)
             ))

#+ecl (main) #+ecl (quit)

(defun arguments (f l)
  (if(not(null(car l)))
    (cond ((and(or(string-equal (car l) "--add") (string-equal (car l) "-a"))
            (not(null(cadr l))) (not(null(caddr l))))
           (tasks-edit f :arg (list (caddr l) (cadr l))))
          
          ((and(or(string-equal (car l) "--edit") (string-equal (car l) "-e"))
             (not(null(cadr l))) (not(null(caddr l))))
           (tasks-edit f :num (sti(cadr l)) :edit (caddr l)))
          
          ((or(string-equal (car l) "--no-color") (string-equal (car l) "-n"))
           (setq *color* nil) (read-file (open f :direction :input)))
          ((and(or(string-equal (car l) "--delete") (string-equal (car l) "-d"))
             (not(null(cadr l))))
           (tasks-edit f :num (sti (cadr l))))
         
          ((or (string-equal (car l) "--help") (string-equal (car l) "-h"))
           (help))
          
          (t (arguments f (cdr l))))
    (read-file (open f :direction :input))))

(defun sti (s)
  (if(integerp s)
    s
    (parse-integer s :junk-allowed t)))


(defun pick-color (n c)
  (let ((s (sti n)))
           ((lambda (x)
              (eval x))
            ;;Change the colors if you want 
            (cond ((and (<= s 20) (/= s 0)) (cadr (assoc "red" c :test #'equalp)))
                  ((and (<= s 40) (> s 20)) (cadr (assoc "yellow" c :test #'equalp)))
                  ((and (<= s 100) (> s 40)) (cadr (assoc "green" c :test #'equalp)))
                  ((and (<= s 250) (> s 100)) (cadr (assoc "cyan" c :test #'equalp)))
                  (t s (cadr (assoc "blue" c :test #'equalp)))))))

(defun read-file (file)
  (format t "ID~cImp~c~cTarea~%" #\tab #\tab #\tab)
  (loop
    for n from 1
    for l = (read-line file nil nil)
    while l
    do (format t "~a~c~a~%" n #\tab ((lambda (x c)
                                       (format nil "~a~a~c~a~a" (if(not(null c))
                                                                  (pick-color (car x) *colors*)
                                                                  "")
                                               (car x) #\tab (cadr x) 
                                               (if (not(null c))
                                                   (eval (cadr (assoc "white" *colors* :test #'equalp)))
                                                   "")))
                                     (strtok l #\|) *color*)))
  (close file))

(defun tasks-edit (file &key num arg edit)
  (cond ((null file) (error "There is no file provided"))
        ((and(not(null edit)) (not(null num))) (let ((f (open file :direction :input)))
                                                 (let ((data (edit-e (get-in-memory f) num edit)))
                                                       (close f)
                                                       (entries data (open file :direction :output :if-exists :supersede)))))
        ((not(null num)) (let ((f (open file :direction :input)))
                           (let ((data (remove-e (get-in-memory f) num)))
                             (close f)
                             (entries data (open file :direction :output :if-exists :supersede)))))
        ((not(null arg)) (with-open-file (f file :direction :output :if-exists :append :if-does-not-exist :create)
                           (format f "~a | ~a~%" (car arg) (cadr arg)))))
  (read-file (open file :direction :input)))
                                                 
(defun remove-e (l del)
  (if (not(null(car l)))
      (if (eql del 1)
          (remove-e (cdr l) 0)
          (cons (car l) (remove-e (cdr l) (1- del))))))

(defun edit-e (l edit new)
  (if(not(null(car l)))
    (if (eql edit 1)
        (rplaca l new)
        (cons (car l) (edit-e (cdr l) (1- edit) new)))))
  
(defun get-in-memory (x)
  (loop
    for l = (read-line x nil nil)
    while l
    collect l))

(defun entries (l dest)
  (if(not(null(car l)))
    (progn (format dest "~a~%" (car l))
    (entries (cdr l) dest))
    (close dest)))

(defun strtok (x del)
  (if(not(null(position del x)))
    (let ((pos (position del x)))
    (cons (subseq x 0 pos) (strtok (subseq x (1+ pos)) del)))
    (cons x nil)))

(defun help ()
  (format t "Modo de uso:
paja: [-a|--add] <tarea> <importancia> [-d|--delete] <id> [-h|--help] [-n|--no-color] [-e|--edit] <id> <\"importancia | tarea\">

        --add           agrega una tarea nueva.
        --delete        Elimina una tarea existente.
        --no-color      Muestra las tareas sin color, por defecto el output es con color.
        --edit          Edita la importancia y el contenido de una tarea existente.
        --help          Muestra este mensaje."))
